const background = document.getElementById('background')
const hoverMeNot = document.getElementById('hover-me-not')
const OFFSET = 10

background.addEventListener('click', (event) => {
    event.target.style.backgroundColor = event.target.style.backgroundColor === 'black' ? 'white' : 'black'
    hoverMeNot.style.color = hoverMeNot.style.color === 'white' ? 'black' : 'white'
    hoverMeNot.style.backgroundColor = hoverMeNot.style.backgroundColor === 'black' ? 'white' : 'black'
})

hoverMeNot.addEventListener('click', () => {
    alert('Nice Try')
    window.close()
})

document.addEventListener('mousemove', (e) => {
    const x = e.pageX
    const y = e.pageY
    const buttonBox = hoverMeNot.getBoundingClientRect()
    const horizontalDistanceFrom = distanceFromCenter(buttonBox.x, x, buttonBox.width)
    const verticalDistanceFrom = distanceFromCenter(buttonBox.y, y, buttonBox.height)
    const horizontalOffset = buttonBox.width / 2 + OFFSET
    const verticalOffset = buttonBox.height / 2 + OFFSET
    if (Math.abs(horizontalDistanceFrom) <= horizontalOffset && Math.abs(verticalDistanceFrom) <= verticalOffset) {
        setButtonPosition(
            buttonBox.x + horizontalOffset / horizontalDistanceFrom * 10 + 10,
            buttonBox.y + verticalOffset / verticalDistanceFrom * 10 + 10
        )
    }
})

function setButtonPosition(left, top) {
    const windowBox = document.body.getBoundingClientRect()
    const buttonBox = hoverMeNot.getBoundingClientRect()

    if (distanceFromCenter(left, windowBox.left, buttonBox.width) < 0) {
        left = windowBox.right - buttonBox.width - OFFSET
    }
    if (distanceFromCenter(left, windowBox.right, buttonBox.width) > 0) {
        left = windowBox.left + OFFSET
    }
    if (distanceFromCenter(top, windowBox.top, buttonBox.height) < 0) {
        top = windowBox.bottom - buttonBox.height - OFFSET
    }
    if (distanceFromCenter(top, windowBox.bottom, buttonBox.height) > 0) {
        top = windowBox.top + OFFSET
    }

    hoverMeNot.style.left = `${left}px`
    hoverMeNot.style.top = `${top}px`
}

function distanceFromCenter(boxPosition, mousePosition, boxSize) {
    return boxPosition - mousePosition + boxSize / 2
}